import { DevicesAction } from '../Actions/Devices.action';

export type SwitchDevice = {
	panelName: string;
	topic: string;
	subscribeTopic?: string;
	payloadOn: string;
	payloadOff: string;
	type: 'switch';
	id: number;
	qos: string;
	idDB: number;
};

export type PulsometerDevice = {
	panelName: string;
	topic: string;
	qos: string;
	type: 'pulsometer';
	id: number;
	idDB: number;
};

export type OximeterDevice = {
	panelName: string;
	topic: string;
	qos: string;
	type: 'oximeter';
	id: number;
	idDB: number;
};

export type CurrentDevice = {
	panelName: string;
	topic: string;
	qos: string;
	type: 'current';
	id: number;
	idDB: number;
};

export type GraphicDevice = {
	panelName: string;
	topic: string;
	qos: string;
	colorGraphic: string;
	type: 'graphic';
	id: number;
	idDB: number;
};

// type IAction = {
// 	type: string;
// 	payload: SwitchDevice;
// };

export type IDeviceState =
	| SwitchDevice
	| PulsometerDevice
	| OximeterDevice
	| CurrentDevice
	| GraphicDevice;

const DevicesReducer = (
	initialState: IDeviceState[] = [],
	action: any,
): IDeviceState[] => {
	//
	switch (action.type) {
		case DevicesAction.REMOVE:
			return initialState.filter((device) => device.id !== action.payload.id);
		case DevicesAction.EDIT:
			return initialState.map((device) => {
				if (device.id === action.payload.id) return action.payload;
				return device;
			});
		case DevicesAction.ADD:
			return [...initialState, { ...action.payload }];
		case DevicesAction.SET:
			return action.payload;
		default:
			return initialState;
	}
};

export default DevicesReducer;
