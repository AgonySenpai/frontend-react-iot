import {
	InMemoryCache,
	ApolloClient,
	HttpLink,
	ApolloLink,
} from '@apollo/client';
import { onError } from '@apollo/client/link/error';

const uriLink = new HttpLink({
	uri: 'http://peru-iot4.com:3001/graphql',
	headers: {
		token: localStorage.getItem('token') || '',
	},
});

const errorLink = onError(({ graphQLErrors }) => {
	if (graphQLErrors) graphQLErrors.map(({ message }) => console.log(message));
});

const client = new ApolloClient({
	link: ApolloLink.from([errorLink, uriLink]),
	cache: new InMemoryCache(),
});

export default client;
