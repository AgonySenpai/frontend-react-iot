import { useMutation } from '@apollo/client';
import React, { FC, memo, useEffect, useRef, useState } from 'react';
import { Line } from 'react-chartjs-2';
import { Col } from 'react-materialize';
import { connect, useDispatch } from 'react-redux';
import DropdownGraphic from '../../Components/Devices/DropdownGraphic';
import { IGraphicState } from '../../Components/Devices/Graphic';
import { Mutation } from '../../GraphQL/QueryMutation';
import { removeGraphic } from '../../Redux/Actions/Graphics.action';
import { IDeviceState } from '../../Redux/Reducers/Devices.reducer';
import { IGraphic } from '../../Redux/Reducers/Graphics.reducer';
import showAlert from '../../Utils/Alert';
import { shortandMonths } from '../../Utils/DateUtils';
import { IOptionRecords } from './AddRecordOptions';
import showAlertAddGraphic from './showAlertAddGraphic';

type IProps = {
	graphic: IGraphic;
	Devices: IDeviceState[];
};

type IMutationRecord = {
	getData: Array<{
		data: number;
		createdAt: string;
		topic: string;
	}>;
};

const Graphic: FC<IProps> = (props) => {
	const { graphic, Devices } = props;
	const dispatch = useDispatch();
	const [graphicInfo, setGraphicInfo] = useState<IOptionRecords | null>(null);
	const [addMutationData, { data }] = useMutation<IMutationRecord>(
		Mutation.getRecordsDevice,
	);
	const lineRef = useRef<Line | null>(null);
	const [graphicState, setGraphicState] = useState<IGraphicState>({
		labels: [],
		datasets: [
			{
				data: [],
				fill: true,
				backgroundColor: 'rgba(255,0,0,0.4)',
				borderColor: 'rgba(255,0,0,1)',
				borderWidth: 3,
			},
		],
	});
	const [width, setWidth] = useState<number>(0);
	const [title, setTitle] = useState('Dispositivo no Configurado');

	const handleRemove = async () => {
		await showAlert().fire({
			icon: 'question',
			title: 'Confirmar',
			text: 'Desea eliminar esta grafica?',
			preConfirm() {
				dispatch(removeGraphic(graphic.id));
			},
		});
	};

	useEffect(() => {
		const fetchInfo = async () => {
			if (graphicInfo) {
				const id = parseInt(graphicInfo.deviceId);
				const infodevice = Devices.filter(
					(mydevice) => mydevice.idDB === id,
				)[0];
				// const offsetHours = new Date().getTimezoneOffset() / 60;
				const date = new Date();
				date.setFullYear(parseInt(graphicInfo.year));
				date.setMonth(parseInt(graphicInfo.month));
				date.setDate(parseInt(graphicInfo.day));
				date.setHours(parseInt(graphicInfo.hour));
				date.setMinutes(0);
				date.setSeconds(0);
				date.setMilliseconds(0);
				console.log({
					date,
					dateUTC: date.toUTCString(),
				});
				await addMutationData({
					variables: {
						topic: infodevice.topic,
						year: date.getUTCFullYear(),
						month: date.getUTCMonth(),
						day: date.getUTCDay(),
						hour: date.getUTCHours(),
					},
				});
			}
		};
		fetchInfo();
	}, [graphicInfo]);

	useEffect(() => {
		if (data) {
			if (data.getData.length === 0) {
				setTitle('Sin Registros');
				setGraphicState({
					labels: [],
					datasets: [
						{
							data: [],
							fill: true,
							backgroundColor: 'rgba(255,0,0,0.4)',
							borderColor: 'rgba(255,0,0,1)',
							borderWidth: 3,
						},
					],
				});
				setWidth(0);
			} else {
				// Create the graphic with information
				const temporalLabels: string[] = [];
				const temporalData: number[] = [];
				data.getData.forEach((data) => {
					const date = new Date(data.createdAt);
					const offsetHours = new Date().getTimezoneOffset() / 60;
					const month = shortandMonths[date.getMonth()];
					const year = date.getFullYear().toString().substr(2);
					const hour = date.getHours() + offsetHours;
					const temporalDate = `${date.getDay()}\\${month}\\${year} ${hour}:${date.getMinutes()}:${date.getSeconds()} `;
					temporalLabels.push(temporalDate);
					temporalData.push(data.data);
				});
				const newInfo = Object.assign({}, graphicState);
				newInfo.labels = temporalLabels;
				newInfo.datasets[0].data = temporalData;
				setGraphicState(newInfo);
				const device = Devices.filter(
					(deviceState) => deviceState.topic === data.getData[0].topic,
				)[0];
				const newTitle = `Dispositivo: ${device.panelName} Fecha: ${
					graphicInfo?.year
				}/${parseInt(graphicInfo!.month) + 1}/${graphicInfo?.day}  ${
					graphicInfo?.hour
				} Hrs`;

				// Save in the Session Storage
				const prevGraphics = sessionStorage.getItem('graphics');

				const newGraphic = {
					id: graphic.id,
					prevState: {
						data: temporalData,
						labels: temporalLabels,
						title: newTitle,
					},
				};

				if (prevGraphics) {
					const prevGraphicsJSON: IGraphic[] = JSON.parse(prevGraphics);

					let found = false;
					prevGraphicsJSON.forEach((prevGraphicJSON, index) => {
						if (prevGraphicJSON.id === graphic.id) {
							prevGraphicsJSON[index] = newGraphic;
							found = true;
						}
					});
					if (!found) {
						prevGraphicsJSON.push(newGraphic);
					}
					sessionStorage.setItem('graphics', JSON.stringify(prevGraphicsJSON));
				} else {
					sessionStorage.setItem('graphics', JSON.stringify([newGraphic]));
				}
				setTitle(newTitle);
			}
		} else {
			setGraphicInfo(null);
		}
	}, [data]);

	const handleEdit = async () => {
		//
		const values: IOptionRecords | undefined = await showAlertAddGraphic(
			Devices,
		);
		if (values) {
			setGraphicInfo(values);
		}
	};

	useEffect(() => {
		// Load the prev information
		if (graphic.prevState) {
			const newInfo = Object.assign({}, graphicState);
			newInfo.labels = graphic.prevState.labels;
			newInfo.datasets[0].data = graphic.prevState.data;
			setTitle(graphic.prevState.title);
			setGraphicState(newInfo);
		}
	}, []);

	useEffect(() => {
		const newWidth = graphicState.datasets[0].data.length;
		if (newWidth >= 100) {
			setWidth(newWidth * 50);
		}
		console.log(newWidth);
	}, [graphicState]);

	return (
		<Col s={12}>
			<div
				className='card hoverable p-relative hoverable'
				style={{ borderRadius: '20px' }}
			>
				<div className='card-content'>
					<h6 className={'card-title text-center'}>{title}</h6>
					<div style={{ overflowX: 'scroll' }}>
						<div style={{ width: `${width === 0 ? 'auto' : width + 'px'}` }}>
							<Line
								data={graphicState}
								options={{ maintainAspectRatio: false, responsive: true }}
								ref={lineRef}
							/>
						</div>
					</div>
				</div>
				<DropdownGraphic
					idTrigger={`graphic-${graphic.id}`}
					handleRemove={handleRemove}
					handleEdit={handleEdit}
				/>
			</div>
		</Col>
	);
};

const mapStateToProps = (state: any) => ({
	Devices: state.DevicesReducer,
});

export default connect(mapStateToProps)(memo(Graphic));
