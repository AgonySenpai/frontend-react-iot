import { useMutation } from '@apollo/client';
import React, { FC, memo } from 'react';
import { Col, Row } from 'react-materialize';
import { connect, useDispatch } from 'react-redux';
import Current from '../../Components/Devices/Current';
import Graphic from '../../Components/Devices/Graphic';
import Oximeter from '../../Components/Devices/Oximeter';
import Pulsometer from '../../Components/Devices/Pulsometer';
import { Mutation } from '../../GraphQL/QueryMutation';
import useGetDevices from '../../Hooks/getDevices';
import { editDevice, removeDevice } from '../../Redux/Actions/Devices.action';
import { IDeviceState } from '../../Redux/Reducers/Devices.reducer';
import Switch from '../../Components/Devices/Switch';
import showAlert from '../../Utils/Alert';
import styles from './Dashboard.module.scss';
import { showEdit } from './ShowEditComponents';

type IProps = {
	Devices: IDeviceState[];
};

export type IDeviceQuery = {
	getDevices: Array<{
		id: number;
		info: string;
	}>;
};

const ListDevices: FC<IProps> = (props) => {
	const { Devices } = props;
	const [addMutationDevice] = useMutation(Mutation.editDevice);
	const [deleteDeviceMutation] = useMutation(Mutation.removeDevice);
	const dispatch = useDispatch();
	useGetDevices();

	const handleEdit = async (device: IDeviceState) => {
		const values = await showEdit(device);
		if (values) {
			await addMutationDevice({
				variables: {
					info: JSON.stringify(values),
				},
			});
			await dispatch(editDevice(values));
		}
	};

	const handleRemove = async (device: IDeviceState) => {
		await showAlert().fire({
			cancelButtonColor: 'red',
			showCancelButton: true,
			icon: 'question',
			title: 'Confirmar',
			text: 'Desea eliminar este dispositivo?',
			async preConfirm() {
				console.log(device);
				await deleteDeviceMutation({
					variables: {
						idDB: device.idDB,
					},
				});
				await dispatch(removeDevice(device.id));
			},
		});
	};

	return (
		<Row className={`${styles.listDevice}`}>
			{Devices.map((device: IDeviceState) => {
				let TemporalComponent;
				if (device.type === 'switch')
					TemporalComponent = (
						<Switch
							Switch={device}
							handleEdit={handleEdit}
							onRemove={handleRemove}
						/>
					);
				else if (device.type === 'pulsometer')
					TemporalComponent = (
						<Pulsometer
							device={device}
							onEdit={handleEdit}
							onRemove={handleRemove}
						/>
					);
				else if (device.type === 'oximeter')
					TemporalComponent = (
						<Oximeter
							Oximeter={device}
							onEdit={handleEdit}
							onRemove={handleRemove}
						/>
					);
				else if (device.type === 'current')
					TemporalComponent = (
						<Current
							current={device}
							onEdit={handleEdit}
							onRemove={handleRemove}
						/>
					);
				else if (device.type === 'graphic')
					TemporalComponent = (
						<Graphic
							graphic={device}
							onEdit={handleEdit}
							onRemove={handleRemove}
						/>
					);
				if (!TemporalComponent) return null;
				return (
					<Col s={12} m={6} l={4} xl={3} key={device.id}>
						{TemporalComponent}
					</Col>
				);
			})}
		</Row>
	);
};

const mapStateToProps = (state: any) => ({
	Devices: state.DevicesReducer,
});

export default connect(mapStateToProps)(memo(ListDevices));
