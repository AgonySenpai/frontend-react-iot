import styled from '@emotion/styled';
import React, {
	FC,
	memo,
	useContext,
	useEffect,
	useRef,
	useState,
} from 'react';
import { Line } from 'react-chartjs-2';
import { SocketsContext } from '../../Hooks/SocketsProvider';
import styles from '../../Pages/Dashboard/Dashboard.module.scss';
import DropdownDevice from './DropdownDevice';
import { GraphicDevice } from '../../Redux/Reducers/Devices.reducer';
import TitleDevice from './TitleDevice';

type IProps = {
	graphic: GraphicDevice;
	onEdit(e: GraphicDevice): void;
	onRemove(e: GraphicDevice): void;
};

export type IGraphicState = {
	labels: Array<string>;
	datasets: Array<{
		data: Array<number>;
		fill: boolean;
		backgroundColor: string;
		borderColor: string;
		borderWidth: number;
	}>;
};

const DivGraphic = styled.div`
	top: 60%;
	left: 50%;
	transform: translateY(-50%) translateX(-50%);
	overflow-y: auto;
	position: relative;
`;

const Graphic: FC<IProps> = (props) => {
	const { graphic, onEdit, onRemove } = props;
	const io = useContext(SocketsContext);
	const [GraphicWidth, setGraphicWidth] = useState(0);
	const [graphicState, setGraphicState] = useState<IGraphicState>({
		labels: [],
		datasets: [
			{
				data: [],
				fill: true,
				backgroundColor: 'rgba(255,0,0,0.4)',
				borderColor: 'rgba(255,0,0,1)',
				borderWidth: 3,
			},
		],
	});
	const lineRef = useRef<Line | null>(null);

	useEffect(() => {
		io.on('newDataDevice', (data: string) => {
			const response = JSON.parse(data);
			if (response.topic === graphic.topic) {
				const time = new Date();
				const newTime = `${time.getHours()}:${time.getMinutes()}:${time.getSeconds()}`;
				const newGraphic = Object.assign({}, graphicState);
				newGraphic.labels.push(newTime);
				newGraphic.datasets[0].data.push(response.data);
				console.log(GraphicWidth);
				setGraphicState(newGraphic);
			}
		});
	}, [graphic.topic, io]);

	useEffect(() => {
		const newWidth = graphicState.datasets[0].data.length;
		if (newWidth > 8) {
			setGraphicWidth(newWidth * 50);
		}
	}, [graphicState]);

	return (
		<div className={`card ${styles.deviceCard} p-relative w-100 hoverable`}>
			<TitleDevice className={'p-absolute'}>{graphic.panelName}</TitleDevice>

			<DivGraphic>
				<div
					style={{
						width: `${GraphicWidth === 0 ? 'auto' : GraphicWidth + 'px'}`,
					}}
				>
					<Line
						data={graphicState}
						redraw
						width={600}
						ref={lineRef}
						options={{
							maintainAspectRatio: false,
							responsive: true,
							animation: false,
						}}
					/>
				</div>
			</DivGraphic>

			<DropdownDevice
				idTrigger={`pulsometer-${graphic.id}`}
				handleRemove={() => onRemove(graphic)}
				handleEdit={() => onEdit(graphic)}
			/>
		</div>
	);
};

export default memo(Graphic);
