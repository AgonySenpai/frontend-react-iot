import { faLungs } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, { FC, memo, useContext, useEffect, useState } from 'react';
import { SocketsContext } from '../../Hooks/SocketsProvider';
import styles from '../../Pages/Dashboard/Dashboard.module.scss';
import DropdownDevice from './DropdownDevice';
import {
	IDeviceState,
	OximeterDevice,
} from '../../Redux/Reducers/Devices.reducer';
import FooterDevice from './FooterDevice';
import TitleDevice from './TitleDevice';

type IProps = {
	Oximeter: OximeterDevice;
	onEdit(e: IDeviceState): void;
	onRemove(e: IDeviceState): void;
};

const Oximeter: FC<IProps> = (props) => {
	const { Oximeter, onEdit, onRemove } = props;
	const [state, setState] = useState();
	const io = useContext(SocketsContext);

	useEffect(() => {
		io.on('newDataDevice', (data: string) => {
			const response = JSON.parse(data);
			if (response.topic === Oximeter.topic) {
				setState(response.data);
			}
		});
	}, [Oximeter.topic, io]);

	return (
		<div className={`card ${styles.deviceCard} p-relative`}>
			<TitleDevice className={'p-absolute'}>{Oximeter.panelName}</TitleDevice>
			<FontAwesomeIcon
				className='p-absolute'
				style={{ left: '50%', top: '50%', transform: 'translate(-50%,-50%)' }}
				icon={faLungs}
				color={'#EA899A'}
				size={'6x'}
			/>
			<FooterDevice className={`p-absolute`}>{state}</FooterDevice>

			<DropdownDevice
				idTrigger={`pulsometer-${Oximeter.id}`}
				handleRemove={() => onRemove(Oximeter)}
				handleEdit={() => onEdit(Oximeter)}
			/>
		</div>
	);
};

export default memo(Oximeter);
