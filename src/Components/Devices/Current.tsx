import { faBolt } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, { FC, memo, useEffect, useState } from 'react';
import { SocketsContext } from '../../Hooks/SocketsProvider';
import styles from '../../Pages/Dashboard/Dashboard.module.scss';
import DropdownDevice from './DropdownDevice';
import { CurrentDevice } from '../../Redux/Reducers/Devices.reducer';
import FooterDevice from './FooterDevice';
import TitleDevice from './TitleDevice';
// import client from 'socket.io-client';

type IProps = {
	current: CurrentDevice;
	onEdit(e: CurrentDevice): void;
	onRemove(e: CurrentDevice): void;
};

const Current: FC<IProps> = (props) => {
	const { current, onEdit, onRemove } = props;
	const [state, setState] = useState();
	const io = React.useContext(SocketsContext);

	useEffect(() => {
		io.on('newDataDevice', (data: string) => {
			const response = JSON.parse(data);
			if (response.topic === current.topic) {
				setState(response.data);
			}
		});
	}, [current.topic, io]);

	return (
		<div className={`card ${styles.deviceCard} p-relative`}>
			<TitleDevice className={'p-absolute'}>{current.panelName}</TitleDevice>
			<FontAwesomeIcon
				className='p-absolute'
				style={{ left: '50%', top: '50%', transform: 'translate(-50%,-50%)' }}
				icon={faBolt}
				color={'#FFFF00'}
				size={'6x'}
			/>
			<FooterDevice className={`p-absolute`}>{state}</FooterDevice>

			<DropdownDevice
				idTrigger={`pulsometer-${current.id}`}
				handleRemove={() => onRemove(current)}
				handleEdit={() => onEdit(current)}
			/>
		</div>
	);
};

export default memo(Current);
