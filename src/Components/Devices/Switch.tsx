import React, { FC, memo, useState } from 'react';
import { Button } from 'react-materialize';
import { SwitchDevice } from '../../Redux/Reducers/Devices.reducer';
import styles from '../../Pages/Dashboard/Dashboard.module.scss';
import DropdownDevice from './DropdownDevice';
import FooterDevice from './FooterDevice';
import TitleDevice from './TitleDevice';

type IProps = {
	Switch: SwitchDevice;
	handleEdit(e: SwitchDevice): void;
	onRemove(e: SwitchDevice): void;
};

const Switch: FC<IProps> = (props) => {
	const { Switch, handleEdit, onRemove } = props;
	const [on, setOn] = useState(false);

	const handleClick = () => {
		setOn(!on);
	};

	return (
		<div className={`card ${styles.deviceCard} p-relative`}>
			<TitleDevice className={'p-absolute'}>{Switch.panelName}</TitleDevice>
			<Button
				onClick={handleClick}
				waves={'light'}
				floating
				className={`${styles.switchButton} ${
					on ? '' : styles.colorPerla
				} p-absolute`}
			>
				<i
					className={`material-icons`}
					style={{
						color: on ? 'white' : 'red',
					}}
				>
					power_settings_new
				</i>
			</Button>
			<FooterDevice className={`p-absolute`}>On</FooterDevice>

			<DropdownDevice
				idTrigger={`switch-${Switch.id}`}
				handleRemove={() => onRemove(Switch)}
				handleEdit={() => handleEdit(Switch)}
			/>
		</div>
	);
};

export default memo(Switch);
