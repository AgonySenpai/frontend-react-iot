import React, { FC, memo, useContext, useEffect, useState } from 'react';
import { SocketsContext } from '../../Hooks/SocketsProvider';
import styles from '../../Pages/Dashboard/Dashboard.module.scss';
import DropdownDevice from './DropdownDevice';
import { PulsometerDevice } from '../../Redux/Reducers/Devices.reducer';
import { faHeartbeat } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import FooterDevice from './FooterDevice';
import TitleDevice from './TitleDevice';

type IProps = {
	device: PulsometerDevice;
	onEdit(e: PulsometerDevice): void;
	onRemove(e: PulsometerDevice): void;
};

const Pulsometer: FC<IProps> = (props) => {
	const { device, onEdit, onRemove } = props;
	const io = useContext(SocketsContext);
	const [state, setState] = useState();

	useEffect(() => {
		io.on('newDataDevice', (data: string) => {
			const response = JSON.parse(data);
			if (response.topic === device.topic) {
				setState(response.data);
			}
		});
	}, [device.topic, io]);

	return (
		<div className={`card ${styles.deviceCard} p-relative`}>
			<TitleDevice className={'p-absolute'}>{device.panelName}</TitleDevice>
			<FontAwesomeIcon
				className='p-absolute'
				style={{ left: '50%', top: '50%', transform: 'translate(-50%,-50%)' }}
				icon={faHeartbeat}
				color={'red'}
				size={'6x'}
			/>
			<FooterDevice className={`p-absolute`}>{state}</FooterDevice>

			<DropdownDevice
				idTrigger={`pulsometer-${device.id}`}
				handleRemove={() => onRemove(device)}
				handleEdit={() => onEdit(device)}
			/>
		</div>
	);
};

export default memo(Pulsometer);
