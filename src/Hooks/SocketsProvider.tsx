import React, { createContext, FC, memo } from 'react';
import io from 'socket.io-client';

const socket = io.connect('http://www.peru-iot4.com:3001', {
	host: 'http://www.peru-iot4.com',
	port: '3001',
});

export const SocketsContext = createContext<SocketIOClient.Socket>(socket);

type SocketsProvider = {
	children: React.ReactElement;
};

const SocketsProvider: FC<SocketsProvider> = (props) => {
	return (
		<SocketsContext.Provider value={socket}>
			{props.children}
		</SocketsContext.Provider>
	);
};

export default memo(SocketsProvider);
